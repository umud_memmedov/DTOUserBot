# Copyright (C) 2019 The Raphielscape Company LLC.
#
# Licensed under the Raphielscape Public License, Version 1.c (the "License");
# you may not use this file except in compliance with the License.
#

# DTÖ UserBot - Ümüd


""" İnternet ilə əlaqəli bilgilər üçün olan DTÖ moduludur. """

from datetime import datetime

from speedtest import Speedtest
from telethon import functions
from userbot import CMD_HELP
from userbot.events import register


@register(outgoing=True, pattern="^.speed$")
async def speedtst(spd):
    """ .speed əmri İnternet sürətinizi yoxlamaq üçün olan moduldur. """
    await spd.edit("`Sürət testi edilir ...`")
    test = Speedtest()

    test.get_best_server()
    test.download()
    test.upload()
    result = test.results.dict()

    await spd.edit("`"
                   "Başlama Tarixi: "
                   f"{result['timestamp']} \n\n"
                   "Yüklənmə sürəti: "
                   f"{speed_convert(result['download'])} \n"
                   "Yükləmə sürəti: "
                   f"{speed_convert(result['upload'])} \n"
                   "Ping: "
                   f"{result['ping']} \n"
                   "İnternet sürət provayderi: "
                   f"{result['client']['isp']}"
                   "`")


def speed_convert(size):
    """
    Salam DTÖUserBot, baytları oxuya bilmir kömək ola bilərsən?
    """
    power = 2**10
    zero = 0
    units = {0: '', 1: 'Kb/s', 2: 'Mb/s', 3: 'Gb/s', 4: 'Tb/s'}
    while size > power:
        size /= power
        zero += 1
    return f"{round(size, 2)} {units[zero]}"


@register(outgoing=True, pattern="^.dc$")
async def neardc(event):
    """ .dc əmri ən yaxın datacenter məlumatını verir. """
    result = await event.client(functions.help.GetNearestDcRequest())
    await event.edit(f"Şəhər : `{result.country}`\n"
                     f"Ən yaxın datacenter : `{result.nearest_dc}`\n"
                     f"İndiki datacenter : `{result.this_dc}`")


@register(outgoing=True, pattern="^.ping$")
async def pingme(pong):
    """ .ping əmri userbotun ping dəyərini hər hansı bir söhbətdə yazaraq baxa bilərsiz.  """
    start = datetime.now()
    await pong.edit("`Pinginiz!`")
    end = datetime.now()
    duration = (end - start).microseconds / 1000
    await pong.edit("`Pinginiz!\n%sms`" % (duration))


CMD_HELP.update(
    {"speed": ".speed\
    \nİşlədilişi: İnternet sürətinizi göstərir"})
CMD_HELP.update(
    {"dc": ".dc\
    \nİşdəlişi: Servərə ən yaxın datacenter'ı göstərir."})
CMD_HELP.update(
    {"ping": ".ping\
    \nİşlədilişi: Botun ping dəyərini göstərir."})
