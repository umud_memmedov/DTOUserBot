<div align="center">
  <img src="https://i.imgyukle.com/2020/09/05/xaXuhA.jpg" width="200" height="200">
  <h1>DTÖ UserBot</h1>
</div>
<p align="center">
    DTÖ UserBot, Telegram işlətməyinizi asandlaşdıran botdur. Tamamilə pulsuzdur.
    <br>
        <a href="https://t.me/DTOUserBot">Telegram qrup</a>
    <br>
</p>

----

## Qurulum
Aşağıdakı heroku logosuna basın və yükləməyə başlayın

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/umudmmmdov1/DTOUserBot)
 
Hər hansı şikayət və təklifləriniz varsa [support qrupuna](https://t.me/DTOSupport) daxil ola bilərsiz.

```
    Userbota göre telegram hesabınız bloklana biler
    Bu bir açıq qaynağlı bir projedi, elediyiniz her birşeyden siz cavabdehsiz. DTÖ Adminleri cavabdeh deil.
    DTÖUserBotu quraraq bütün öhdeliklikleri qebul etmiş sayılırsız.
```

## Credit
Thanks for;

[RaphielGang for creating Telegram-Userbot](https://github.com/RaphielGang)

[Tdug Team for Translations](https://github.com/TeamDerUntergang)

[Spechide for Inline Bot Plugin](https://github.com/Spechide)
