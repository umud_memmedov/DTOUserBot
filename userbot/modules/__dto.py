# Copyright (C) 2019 The Raphielscape Company LLC.
#
# Licensed under the Raphielscape Public License, Version 1.c (the "License");
# you may not use this file except in compliance with the License.
#

# DTÖ UserBot - Ümüd
#

""" DTÖ UserBot kömək əmri"""

from userbot import CMD_HELP
from userbot.events import register

@register(outgoing=True, pattern="^.dto(?: |$)(.*)")
async def dto(event):
    """ .dto əmri üçün """
    args = event.pattern_match.group(1).lower()
    if args:
        if args in CMD_HELP:
            await event.edit(str(CMD_HELP[args]))
        else:
            await event.edit("Zəhmət olmasa bir DTÖ modulu yazın.")
    else:
        await event.edit("Zəhmət olmasa DTÖ modul köməyi üçün modul adı yazın !!\
            \nİşlədilişi: .dto <modul adı>")
        string = ""
        for i in CMD_HELP:
            string += "`" + str(i)
            string += "`\n"
        await event.reply(string)
